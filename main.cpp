#include <QtGui/QApplication>
#include "qmlapplicationviewer.h"
#include "applicationdata.h"
#include <QSettings>
#include <QtDeclarative>

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QmlApplicationViewer viewer; // = QmlApplicationViewer::create();

    ApplicationData appData;
    appData.initialize();

    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationLockPortrait);
    viewer.rootContext()->setContextProperty("appData", &appData);
    viewer.setMainQmlFile(QLatin1String("qml/SAS/main.qml"));
    viewer.showExpanded();

    return app.exec();
}
