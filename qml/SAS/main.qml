import QtQuick 1.1
import com.nokia.meego 1.0

/**
 * Harmattan view which provides control facilities for
 * SAS Check-In application. This embeds {@link SASCI}, an about
 * screen and a busy indicator.
 *
 * @author Copyright (c) Andrew Flegg 2012. Released under the Artistic Licence.
 */
PageStackWindow {
    id: appWindow

    showStatusBar: true
    initialPage: mainPage

    Page {
        id: mainPage
        tools: commonTools
        orientationLock: PageOrientation.LockPortrait

        Flickable {
            id: flickable
            width: parent.width
            anchors.fill: parent
            pressDelay: 200
            onMovementStarted: webView.renderingEnabled = false;
            onMovementEnded: webView.renderingEnabled = true;

            SASCI {
                id: webView
                targetWidth: mainPage.width
                targetHeight: mainPage.height
                onUrlChanged: flickable.contentY = 0
                onContentsSizeChanged: {
                    flickable.contentWidth = parent.width
                    flickable.contentHeight = contentsSize.height
                    console.log(flickable.contentWidth + "\t" + flickable.contentHeight)
                }

                onSelectOpened: flickable.interactive = false
                onSelectClosed: flickable.interactive = true
            }
        }
    }

    ToolBarLayout {
        id: commonTools
        visible: true

        ToolIcon {
            BusyIndicator {
                id: busy
                running: false
                visible: false
                anchors.centerIn: parent

                Connections {
                    target: webView
                    onLoadStarted: {
                        busy.running = true
                        busy.visible = true
                    }
                    onLoadFinished: {
                        busy.running = false
                        busy.visible = false
                    }
                    onLoadFailed: busy.running = false
                }
            }
        }

        ToolIcon {
            platformIconId: "toolbar-view-menu"
            anchors.right: (parent === undefined) ? undefined : parent.right
            onClicked: (menu.status == DialogStatus.Closed) ? menu.open() : menu.close()
        }
    }

    Menu {
        id: menu
        visualParent: pageStack
        MenuLayout {
            MenuItem {
                text: qsTr("Clear saved data")
                onClicked: webView.clear()
                enabled: (about.status == DialogStatus.Closed)
            }

            MenuItem {
                text: qsTr("About");
                onClicked: (about.status == DialogStatus.Closed) ? about.open() : about.close()
            }
        }
    }

    QueryDialog {
        id: about
        visualParent: mainPage
        titleText: "SAS Check-in"
        message: "Quick check-in for SAS flights<br><br>" +
                 "<small>Copyright &copy; Jaffa Software 2012<br>" +
                 "Released under the Artistic Licence<br></small><br>" +
                 "http://www.jaffasoft.co.uk/m/"
        acceptButtonText: qsTr("Close")
    }
}
