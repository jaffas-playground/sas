import QtQuick 1.0
import QtWebKit 1.0

/**
 * The browser instance which controls the SAS check-in application.
 *
 * @author Copyright (c) Andrew Flegg 2012. Released under the Artistic Licence.
 */
WebView {
    id: webView

    property int targetWidth: parent.width
    property int targetHeight: parent.height
    signal selectOpened()
    signal selectClosed()

    transformOrigin: Item.TopLeft

    preferredWidth: targetWidth
    preferredHeight: targetHeight
    contentsScale: 2
    smooth: false

    property bool autoSubmitted: false

    Component.onCompleted: restart();

    /**
     * Reset the browser and reload the homepage.
     */
    function restart() {
        webView.stop.trigger()
        webView.url = "https://ci.sas.mobi/ci/"
    }


    /**
     * When a page is finished loading, register appropriate handlers.
     */
    onLoadFinished: {
        console.log("+++ Page [" + url + "] loaded")
        evaluateJavaScript("window.qmlbridge.loginPage = document.getElementById('frmCheckin') != null")
        evaluateJavaScript("window.qmlbridge.errorMessage = document.getElementById('myError_lblErrorMessage').innerHTML")

        // -- For some reason, we don't get the right styling for <SELECT />s. Allow disabling of flickability...
        //
        evaluateJavaScript("var nodes = document.getElementsByTagName('select'); for (i = 0; i < nodes.length; i++) { nodes[i].onfocus = window.qmlbridge.onSelectFocused; nodes[i].onblur = window.qmlbridge.onSelectBlurred; }")

        // -- Add any CSS we want...
        //
        evaluateJavaScript("var css = document.createElement('style'); css.type = 'text/css'; " +
                           "var styles = 'form>div>center, form>div>span>center { text-align: left; }'; " +
                           "if (css.styleSheet) css.styleSheet.cssText = styles; " +
                           "else css.appendChild(document.createTextNode(styles)); " +
                           "document.getElementsByTagName('head')[0].appendChild(css)");

        if (bridge.loginPage) {
            evaluateJavaScript("document.getElementById('rbCheckInPnr').disabled = true")
            if (bridge.lastName && bridge.cardTypeIndex && bridge.cardNumber) {
                console.log("Populating pre-saved information")
                evaluateJavaScript("document.frmCheckin.txtDefaultLastName.value = window.qmlbridge.lastName")
                evaluateJavaScript("document.frmCheckin.ddCardTypes.selectedIndex = window.qmlbridge.cardTypeIndex")
                evaluateJavaScript("document.frmCheckin.txtSearchCriteria.value = window.qmlbridge.cardNumber")
                //evaluateJavaScript("document.frmCheckin.txtSearchCriteria.type = 'number'")

                if (!bridge.errorMessage && !autoSubmitted) {
                    console.log("Auto-submitting")
                    evaluateJavaScript("document.frmCheckin.btnContinue.click()")
                    autoSubmitted = true
                    return;
                }
            }

            if (autoSubmitted) console.log("Something went wrong...")

            console.log("Registering handler to capture details")
            evaluateJavaScript("document.getElementById('frmCheckin').onsubmit = window.qmlbridge.onSubmit");
            autoSubmitted = false
        }

        if (bridge.errorMessage) console.log("Error [" + bridge.errorMessage + "]")
        bridge.errorMessage = ""
    }

    onAlert: console.log("JS alert: " + message)

    javaScriptWindowObjects: QtObject {
        id: bridge
        WebView.windowObjectName: "qmlbridge"

        property bool loginPage
        property string errorMessage

        property string lastName: appData.lastName
        property int cardTypeIndex: appData.cardTypeIndex
        property string cardNumber: appData.cardNumber
/* These don't work, for some reason, see QTBUG-23394 */
//                    property alias lastName: appData.lastName
//                    property alias cardTypeIndex: appData.cardTypeIndex
//                    property alias cardNumber: appData.cardNumber

        /**
         * Handle submission of the check-in form. This reads the values from
         * the form fields, and updates this object.
         */
        function onSubmit(event) {
            // We need this for some race condition, see QTBUG-23392
            webView.evaluateJavaScript("alert(document.frmCheckin.txtDefaultLastName.value)")

            // Save off the values
            webView.evaluateJavaScript("window.qmlbridge.lastName = document.frmCheckin.txtDefaultLastName.value")
            webView.evaluateJavaScript("window.qmlbridge.cardTypeIndex = document.frmCheckin.ddCardTypes.selectedIndex")
            webView.evaluateJavaScript("window.qmlbridge.cardNumber = document.frmCheckin.txtSearchCriteria.value")
            console.log("Captured - name = [" + lastName + "], card type = [" + cardTypeIndex + "], card number = [" + cardNumber + "]")

            webView.updateSettings()
        }

        /**
         * Called when a SELECT element gets focus.
         */
        function onSelectFocused(event) {
            webView.selectOpened()
        }

        /**
         * Called when a SELECT element loses focus.
         */
        function onSelectBlurred(event) {
            webView.selectClosed()
        }
    }


    /**
     * Clear saved values and restart.
     */
    function clear() {
        bridge.lastName = ''
        bridge.cardTypeIndex = 0
        bridge.cardNumber = ''

        updateSettings()
        restart()
    }


    /**
     * Copy settings to/from the Bridge and settings.
     */
    function updateSettings() {
        // Update saved values
        appData.lastName = bridge.lastName
        appData.cardTypeIndex = bridge.cardTypeIndex
        appData.cardNumber = bridge.cardNumber
    }
}
