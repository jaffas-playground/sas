#ifndef APPLICATIONDATA_H
#define APPLICATIONDATA_H

#include <QSettings>

class ApplicationData : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString lastName READ lastName WRITE setLastName NOTIFY lastNameChanged)
    Q_PROPERTY(int cardTypeIndex READ cardTypeIndex WRITE setCardTypeIndex NOTIFY cardTypeIndexChanged)
    Q_PROPERTY(QString cardNumber READ cardNumber WRITE setCardNumber NOTIFY cardNumberChanged)
public:
    void initialize() {
        QSettings settings("jaffa","SAS");
        _lastName = settings.value("general/lastName", "").toString();
        _cardTypeIndex = settings.value("general/cardTypeIndex", 0).toInt();
        _cardNumber = settings.value("general/cardNumber", "").toString();
    }

    QString lastName() {
        return _lastName;
    }

    int cardTypeIndex() {
        return _cardTypeIndex;
    }

    QString cardNumber() {
        return _cardNumber;
    }

    void setLastName(const QString name) {
        if (name != _lastName) {
            QSettings settings("jaffa","SAS");
            _lastName = name;
            settings.setValue("general/lastName", name);
            emit lastNameChanged();
        }
    }

    void setCardTypeIndex(const int index) {
        if (index != _cardTypeIndex) {
            QSettings settings("jaffa","SAS");
            _cardTypeIndex = index;
            settings.setValue("general/cardTypeIndex", index);
            emit cardTypeIndexChanged();
        }
    }

    void setCardNumber(const QString number) {
        if (number != _cardNumber) {
            QSettings settings("jaffa","SAS");
            _cardNumber = number;
            settings.setValue("general/cardNumber", number);
            emit cardNumberChanged();
        }
    }

signals:
    void lastNameChanged();
    void cardTypeIndexChanged();
    void cardNumberChanged();
private:
    QString _lastName;
    int _cardTypeIndex;
    QString _cardNumber;
};

#endif //APPLICATIONDATA_H
